import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SwiperOptions } from 'swiper';

import SwiperCore, { Zoom } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
SwiperCore.use([Zoom])

@Component({
  selector: 'app-ohiggins-modal',
  templateUrl: './ohiggins-modal.page.html',
  styleUrls: ['./ohiggins-modal.page.scss'],
})
export class OhigginsModalPage implements OnInit {
  @ViewChild('swiper') swiper: SwiperComponent;

  @Input()img: string;
  config: SwiperOptions = {
    zoom: {
      maxRatio: 4,
      minRatio: 0.2
    }
  };

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  zoom(zoomIn) {
    const zoom = this.swiper.swiperRef.zoom;
    zoomIn ? zoom.in() : zoom.out();
  }

  close() {
    this.modalCtrl.dismiss();
  }

}

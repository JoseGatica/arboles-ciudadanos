import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OhigginsModalPage } from './ohiggins-modal.page';

const routes: Routes = [
  {
    path: '',
    component: OhigginsModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OhigginsModalPageRoutingModule {}

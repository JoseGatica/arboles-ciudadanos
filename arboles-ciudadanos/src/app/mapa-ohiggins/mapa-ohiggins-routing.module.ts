import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapaOhigginsPage } from './mapa-ohiggins.page';

const routes: Routes = [
  {
    path: '',
    component: MapaOhigginsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapaOhigginsPageRoutingModule {}

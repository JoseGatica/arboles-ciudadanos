import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapaOhigginsPageRoutingModule } from './mapa-ohiggins-routing.module';

import { MapaOhigginsPage } from './mapa-ohiggins.page';

import { SwiperModule } from 'swiper/angular';
import { MapaBonillaPageRoutingModule } from '../mapa-bonilla/mapa-bonilla-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapaOhigginsPageRoutingModule,
    MapaBonillaPageRoutingModule,
    SwiperModule
  ],
  declarations: [MapaOhigginsPage]
})
export class MapaOhigginsPageModule {}

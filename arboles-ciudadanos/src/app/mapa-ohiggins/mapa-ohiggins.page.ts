import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SwiperOptions } from 'swiper';

import { OhigginsModalPage } from '../ohiggins-modal/ohiggins-modal.page';
import { BonillaModalPage } from '../bonilla-modal/bonilla-modal.page';

@Component({
  selector: 'app-mapa-ohiggins',
  templateUrl: './mapa-ohiggins.page.html',
  styleUrls: ['./mapa-ohiggins.page.scss'],
})
export class MapaOhigginsPage implements OnInit {

  config: SwiperOptions = {
    slidesPerView: 2,
    spaceBetween: 15,
    centeredSlides: true
  };

  constructor(private modalCtrl: ModalController) { }

  async openPreview(img) {
    const modal = await this.modalCtrl.create({
      component: OhigginsModalPage,
      componentProps: {
        img
      },
      cssClass: 'transparent-modal'
    });
    modal.present();
  }

  async openPreview2(img2) {
    const modal2 = await this.modalCtrl.create({
      component: BonillaModalPage,
      componentProps: {
        img2
      },
      cssClass: 'transparent-modal'
    });
    modal2.present();
  }

  ngOnInit() {
  }

}

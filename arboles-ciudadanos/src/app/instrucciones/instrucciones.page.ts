import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instrucciones',
  templateUrl: './instrucciones.page.html',
  styleUrls: ['./instrucciones.page.scss'],
})
export class InstruccionesPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  playSound(){
    let audio = new Audio();
    audio.src = "../assets/audio/audioInstrucciones.mp3";
    audio.load();
    audio.play();
  }

  ionViewDidLeave: true;

}

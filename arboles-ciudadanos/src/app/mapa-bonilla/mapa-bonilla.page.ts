import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SwiperOptions } from 'swiper';

import { BonillaModalPage } from '../bonilla-modal/bonilla-modal.page';

@Component({
  selector: 'app-mapa-bonilla',
  templateUrl: './mapa-bonilla.page.html',
  styleUrls: ['./mapa-bonilla.page.scss'],
})
export class MapaBonillaPage implements OnInit {

  config: SwiperOptions = {
    slidesPerView: 2,
    spaceBetween: 15,
    centeredSlides: true
  };

  constructor(private modalCtrl: ModalController) { }

  async openPreview(img) {
    const modal = await this.modalCtrl.create({
      component: BonillaModalPage,
      componentProps: {
        img
      },
      cssClass: 'transparent-modal'
    });
    modal.present();
  }

  ngOnInit() {
  }

}

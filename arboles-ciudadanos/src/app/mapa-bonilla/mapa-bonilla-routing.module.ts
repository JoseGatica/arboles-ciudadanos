import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapaBonillaPage } from './mapa-bonilla.page';

const routes: Routes = [
  {
    path: '',
    component: MapaBonillaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapaBonillaPageRoutingModule {}

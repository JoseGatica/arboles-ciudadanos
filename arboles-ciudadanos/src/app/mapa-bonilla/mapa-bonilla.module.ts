import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapaBonillaPageRoutingModule } from './mapa-bonilla-routing.module';

import { MapaBonillaPage } from './mapa-bonilla.page';

import { SwiperModule } from 'swiper/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SwiperModule,
    MapaBonillaPageRoutingModule
  ],
  declarations: [MapaBonillaPage]
})
export class MapaBonillaPageModule {}
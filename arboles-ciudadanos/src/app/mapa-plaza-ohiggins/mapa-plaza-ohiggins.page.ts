import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapa-plaza-ohiggins',
  templateUrl: './mapa-plaza-ohiggins.page.html',
  styleUrls: ['./mapa-plaza-ohiggins.page.scss'],
})
export class MapaPlazaOhigginsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  playPeumo(){
    let audio = new Audio();
    audio.src = "/assets/audio/audioInstrucciones.mp3";
    audio.load();
    audio.play();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapaPlazaOhigginsPageRoutingModule } from './mapa-plaza-ohiggins-routing.module';

import { MapaPlazaOhigginsPage } from './mapa-plaza-ohiggins.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapaPlazaOhigginsPageRoutingModule
  ],
  declarations: [MapaPlazaOhigginsPage]
})
export class MapaPlazaOhigginsPageModule {}

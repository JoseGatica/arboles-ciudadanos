import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapaPlazaOhigginsPage } from './mapa-plaza-ohiggins.page';

const routes: Routes = [
  {
    path: '',
    component: MapaPlazaOhigginsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapaPlazaOhigginsPageRoutingModule {}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapaPlazaOhigginsPage } from './mapa-plaza-ohiggins.page';

describe('MapaPlazaOhigginsPage', () => {
  let component: MapaPlazaOhigginsPage;
  let fixture: ComponentFixture<MapaPlazaOhigginsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaPlazaOhigginsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapaPlazaOhigginsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

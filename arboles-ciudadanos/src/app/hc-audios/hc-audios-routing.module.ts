import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HcAudiosPage } from './hc-audios.page';

const routes: Routes = [
  {
    path: '',
    component: HcAudiosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HcAudiosPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HcAudiosPageRoutingModule } from './hc-audios-routing.module';

import { HcAudiosPage } from './hc-audios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HcAudiosPageRoutingModule
  ],
  declarations: [HcAudiosPage]
})
export class HcAudiosPageModule {}

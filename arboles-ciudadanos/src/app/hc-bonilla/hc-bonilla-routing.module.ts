import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HcBonillaPage } from './hc-bonilla.page';

const routes: Routes = [
  {
    path: '',
    component: HcBonillaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HcBonillaPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HcBonillaPageRoutingModule } from './hc-bonilla-routing.module';

import { HcBonillaPage } from './hc-bonilla.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HcBonillaPageRoutingModule
  ],
  declarations: [HcBonillaPage]
})
export class HcBonillaPageModule {}

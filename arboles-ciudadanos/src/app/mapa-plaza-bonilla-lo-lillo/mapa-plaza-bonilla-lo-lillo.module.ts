import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapaPlazaBonillaLoLilloPageRoutingModule } from './mapa-plaza-bonilla-lo-lillo-routing.module';

import { MapaPlazaBonillaLoLilloPage } from './mapa-plaza-bonilla-lo-lillo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapaPlazaBonillaLoLilloPageRoutingModule
  ],
  declarations: [MapaPlazaBonillaLoLilloPage]
})
export class MapaPlazaBonillaLoLilloPageModule {}

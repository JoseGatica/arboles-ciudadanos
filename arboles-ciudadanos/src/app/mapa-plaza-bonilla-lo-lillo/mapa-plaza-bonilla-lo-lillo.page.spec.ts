import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapaPlazaBonillaLoLilloPage } from './mapa-plaza-bonilla-lo-lillo.page';

describe('MapaPlazaBonillaLoLilloPage', () => {
  let component: MapaPlazaBonillaLoLilloPage;
  let fixture: ComponentFixture<MapaPlazaBonillaLoLilloPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaPlazaBonillaLoLilloPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapaPlazaBonillaLoLilloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapa-plaza-bonilla-lo-lillo',
  templateUrl: './mapa-plaza-bonilla-lo-lillo.page.html',
  styleUrls: ['./mapa-plaza-bonilla-lo-lillo.page.scss'],
})
export class MapaPlazaBonillaLoLilloPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  playSound(){
    let audio = new Audio();
    audio.src = "/assets/audio/OLIVO1.wav";
    audio.load();
    audio.play();
  }

}

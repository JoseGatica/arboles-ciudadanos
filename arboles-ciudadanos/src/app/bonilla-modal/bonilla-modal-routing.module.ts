import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BonillaModalPage } from './bonilla-modal.page';

const routes: Routes = [
  {
    path: '',
    component: BonillaModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BonillaModalPageRoutingModule {}

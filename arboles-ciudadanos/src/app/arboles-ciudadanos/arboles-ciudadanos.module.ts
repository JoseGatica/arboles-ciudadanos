import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArbolesCiudadanosPageRoutingModule } from './arboles-ciudadanos-routing.module';

import { ArbolesCiudadanosPage } from './arboles-ciudadanos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArbolesCiudadanosPageRoutingModule
  ],
  declarations: [ArbolesCiudadanosPage]
})
export class ArbolesCiudadanosPageModule {}

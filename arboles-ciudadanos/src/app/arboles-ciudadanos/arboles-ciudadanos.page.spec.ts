import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ArbolesCiudadanosPage } from './arboles-ciudadanos.page';

describe('ArbolesCiudadanosPage', () => {
  let component: ArbolesCiudadanosPage;
  let fixture: ComponentFixture<ArbolesCiudadanosPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbolesCiudadanosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ArbolesCiudadanosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArbolesCiudadanosPage } from './arboles-ciudadanos.page';

const routes: Routes = [
  {
    path: '',
    component: ArbolesCiudadanosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArbolesCiudadanosPageRoutingModule {}

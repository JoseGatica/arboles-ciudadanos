import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HcOhigginsPageRoutingModule } from './hc-ohiggins-routing.module';

import { HcOhigginsPage } from './hc-ohiggins.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HcOhigginsPageRoutingModule
  ],
  declarations: [HcOhigginsPage]
})
export class HcOhigginsPageModule {}

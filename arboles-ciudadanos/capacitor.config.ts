import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'arboles.ciudadanos.app',
  appName: 'Árboles Ciudadanos',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
